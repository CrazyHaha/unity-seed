# Unity-Seed

#### 介绍

用 Unity 复刻 Ludum Dare 第44届72小时多人组的作品 Seed，实现游戏的核心玩法

#### 软件版本

Unity 2019.4.28f1c1 (64-bit)

Visual Studio 2019

#### 原作链接

作者：AVAVT

链接：https://avavt.itch.io/seed

介绍：《Seed》是第44届72小时多人组的作品

比赛主题：“你的生命是货币”

而《Seed》说：“‘你’的生命，是‘人类’的货币“

#### 游戏玩法

游戏的玩法就是“种树”：

在人类的砍伐中努力的种树，
当这片土地上的最后一颗树被人类砍掉后，土地上的一切将快速的消亡，
等一切全部消亡后又会回到最初的样子重新开始

# 游戏效果呈现

![Image text](https://gitee.com/CrazyHaha/unity-seed/raw/master/EffectImages/SeedDemo.gif)

# 游戏效果图

游戏具体参数图
![Image text](https://gitee.com/CrazyHaha/unity-seed/raw/master/EffectImages/Seed.jpg)
原作效果图
![Image text](https://gitee.com/CrazyHaha/unity-seed/raw/master/EffectImages/Seed_01.png)
![Image text](https://gitee.com/CrazyHaha/unity-seed/raw/master/EffectImages/Seed_02.png)

# 更新实现进度

#### 11-14 更新推送

美术素材整理：整理游戏中种子、树木以及人类的图片等素材

音效素材整理：背景音乐、种植音效以及人类砍树等游戏音效

#### 11-15 更新推送

游戏动画：制作各类种子的成长动画、被攻击动画以及被砍掉的动画

游戏中的树木：完成各类型种子的种植逻辑，以及种子种植过程中的变异可能性逻辑

#### 11-16 更新推送

游戏中的人类：制作人类行走、攻击、回家以及死亡动画；完成人类各个状态下的行动交互逻辑

人类&树木：完成人类和树木的相互关联交互逻辑（人砍伐树木，部分树木攻击人类）

游戏进程：完成游戏开始、人类产生、人类老化以及游戏结束的相关进程逻辑

#### 最后

本项目只完成了游戏的核心部分玩法，对游戏的一些功能进行了删减，并未全部复刻
