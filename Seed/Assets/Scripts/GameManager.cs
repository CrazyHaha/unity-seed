﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// 游戏管理器
/// </summary>
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    [Header("游戏状态")]
    // 游戏是否开始
    public bool isStart;
    // 游戏是否结束
    public bool isOver;
    // 是否处于准备状体
    public bool isReady;
    // 是否可以种植
    public bool isPlant;
    // 是否开始生成人类
    public bool isGenerateHuman;

    [Header("游戏时间")]
    public float gameTimer;

    [Header("游戏参数")]
    // 木头数量
    public int wood = 0;
    // 人类价值
    public int humanPrice = 10;

    [Header("种子选项列表")]
    public SeedBtn[] seedBtns;
    [Header("种植的树")]
    public Transform PlantTreesPos;
    public List<GameObject> plantTrees;
    [Header("当前拥有的树类型")]
    public List<SeedType> ownedTreeType;
    [Header("当前的人类")]
    public Transform humanPos;
    public Transform humanHomePos;
    public List<GameObject> humans;

    [Header("顶部UI")]
    // 种植的树木数量
    public Text treeText;
    // 收获的数目数量
    public Text woodText;
    // 顶部提示
    public Animation tipsAni;
    public Text tipsText;

    [Header("游戏遮罩")]
    public GameObject overMask;

    private void Awake()
    {
        Instance = this;

        // 初始化设置
        isStart = false;
        isReady = false;
        gameTimer = 0;
        for (int i = 1; i < seedBtns.Length; i++)
        {
            // 隐藏其他种子选项
            seedBtns[i].gameObject.SetActive(false);
        }
        // 添加拥有的树类型
        ownedTreeType.Add(SeedType.FristSeed);
        Invoke("GameAwake", 1.5f);
    }

    private void Update()
    {
        // 种子种植
        if (isReady)
        {
            // 判断是否可以种植
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            // 第一颗种子
            if (!isStart && hit.collider != null)
            {
                if (hit.collider.tag == "Sand" && hit.point.y < 2.3f && hit.point.y > -2f)
                {
                    if (!isPlant)
                        isPlant = true;
                    if (Input.GetMouseButtonDown(0))
                    {
                        isStart = true;
                        isReady = false;
                        isPlant = false;
                        // 实例化第一颗树
                        Vector3 treePos = new Vector3(hit.point.x, hit.point.y, hit.point.y);
                        GameObject tree = Instantiate(Resources.Load<GameObject>("Prefabs/FirstTree"), treePos, Quaternion.identity);
                        tree.transform.SetParent(PlantTreesPos);
                        ownedTreeType.Add(SeedType.Linbum);
                        // 开启CD和提示
                        seedBtns[0].StartCD();
                        TriggerTipsAni("你在这片土地上种下了第一颗种子");
                        AudioManager.Instance.PlayGameSound(AudioManager.Instance.auioClips[0]);
                    }
                }
                else
                {
                    if (Input.GetMouseButtonDown(0))
                        TriggerTipsAni("你不能在这里进行种植");
                    if (isPlant)
                        isPlant = false;
                }
            }
            // 其他种子
            else if (isStart && hit.collider != null)
            {
                if (hit.collider.tag == "Green" && hit.point.y < 2.3f && hit.point.y > -2f)
                {
                    if (!isPlant)
                        isPlant = true;
                    if (Input.GetMouseButtonDown(0))
                    {
                        isReady = false;
                        isPlant = false;
                        // 是否变异
                        SeedType type = IsVariation(SeedItemBar.Instance.CurrSeedType);
                        // 实例化其他树
                        Vector3 treePos = new Vector3(hit.point.x, hit.point.y, hit.point.y);
                        GameObject tree = Instantiate(Resources.Load<GameObject>("Prefabs/" + type), treePos, Quaternion.identity);
                        tree.transform.SetParent(PlantTreesPos);
                        // 开启CD
                        SeedBtnCD(SeedItemBar.Instance.CurrSeedType);
                        AudioManager.Instance.PlayGameSound(AudioManager.Instance.auioClips[1]);
                    }
                }
                else
                {
                    if (Input.GetMouseButtonDown(0))
                        TriggerTipsAni("你不能在这里进行种植");
                    if (isPlant)
                        isPlant = false;
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                    TriggerTipsAni("你不能在这里进行种植");
                if (isPlant)
                    isPlant = false;
            }
        }

        // 游戏时间
        if (isStart && !isOver)
        {
            OnGameTimer();
        }

        // 游戏结束
        if (isStart && gameTimer > 5 && plantTrees.Count == 0 && !isOver)
        {
            isOver = true;
            TriggerTipsAni("人类砍倒了这片土地上的最后一棵树");
            // 加速人类老化
            foreach (GameObject g in humans)
            {
                g.GetComponent<Human>().QuickAging();
            }
        }
        if (isStart && isOver && humans.Count == 0 && !overMask.activeSelf)
        {
            TriggerTipsAni("这片土地又回到了最初的样子");
            Invoke("GameOver", 1f);
        }

        // 顶部UI内容更新
        if (treeText.text != plantTrees.Count.ToString())
        {
            treeText.text = plantTrees.Count.ToString();
        }
        if (woodText.text != wood.ToString())
        {
            woodText.text = wood.ToString();
            // 判断是否生成新的人类
            Invoke("BuyHuman", 0.5f);
        }
    }

    /// <summary>
    /// 购买新的人类
    /// </summary>
    public void BuyHuman()
    {
        if (isOver)
            return;
        if (wood / humanPrice != 0)
        {
            int num = wood / humanPrice;
            for (int i = 0; i < num; i++)
            {
                StartCoroutine(GenerateHuman(0));
                AudioManager.Instance.PlayGameSound(AudioManager.Instance.auioClips[5]);
            }
            wood = wood % humanPrice;
        }
    }

    /// <summary>
    /// 触发提示动画
    /// </summary>
    public void TriggerTipsAni(string tips)
    {
        tipsText.text = tips;
        tipsAni.Play();
    }

    /// <summary>
    /// 开启选项按钮CD
    /// </summary>
    private void SeedBtnCD(SeedType seedType)
    {
        switch (seedType)
        {
            case SeedType.Linbum:
                seedBtns[0].StartCD();
                break;
            case SeedType.Linbum2:
                seedBtns[1].StartCD();
                break;
            case SeedType.Blumen:
                seedBtns[2].StartCD();
                break;
            case SeedType.StoneTree:
                seedBtns[3].StartCD();
                break;
            case SeedType.Diva:
                seedBtns[4].StartCD();
                break;
        }
    }

    /// <summary>
    /// 游戏计时器
    /// </summary>
    private void OnGameTimer()
    {
        gameTimer += Time.deltaTime;
        if (isStart && !isGenerateHuman)
        {
            StartCoroutine(GenerateHuman(5));
        }
    }

    /// <summary>
    /// 是否发生变异（树）
    /// </summary>
    private SeedType IsVariation(SeedType seedType)
    {
        SeedType type = seedType;
        // 种子类型的变异机制
        int num = Random.Range(1, 4);
        bool isVariation = false;
        switch (num)
        {
            case 1:
                isVariation = true;
                break;
            default:
                isVariation = false;
                break;
        }
        // 没有发生变异
        if (!isVariation)
            return seedType;
        // 发生变异
        if (!IsOwned(SeedType.Blumen))
        {
            // 变异：Blumen
            seedType = SeedType.Blumen;
            if (!seedBtns[2].gameObject.activeSelf)
            {
                seedBtns[2].gameObject.SetActive(true);
                seedBtns[2].StartCD();
            }
        }
        else
        {
            if (!IsOwned(SeedType.StoneTree))
            {
                // 变异：StoneTree
                seedType = SeedType.StoneTree;
                if (!seedBtns[3].gameObject.activeSelf)
                {
                    seedBtns[3].gameObject.SetActive(true);
                    seedBtns[3].StartCD();
                }
            }
            else
            {
                if (!IsOwned(SeedType.Diva))
                {
                    // 变异：Diva
                    seedType = SeedType.Diva;
                    if (!seedBtns[4].gameObject.activeSelf)
                    {
                        seedBtns[4].gameObject.SetActive(true);
                        seedBtns[4].StartCD();
                    }
                }
                else
                {
                    if (!IsOwned(SeedType.Linbum2))
                    {
                        // 变异：Linbum2
                        seedType = SeedType.Linbum2;
                        if (!seedBtns[1].gameObject.activeSelf)
                        {
                            seedBtns[1].gameObject.SetActive(true);
                            seedBtns[1].StartCD();
                        }
                    }
                }
            }
        }
        // 结果
        if (seedType != type)
        {
            ownedTreeType.Add(seedType);
            TriggerTipsAni("你的种子正在发生变异");
            AudioManager.Instance.PlayGameSound(AudioManager.Instance.auioClips[2]);
            return seedType;
        }
        else
        {
            return type;
        }
    }

    /// <summary>
    /// 树类型是否已存在
    /// </summary>
    private bool IsOwned(SeedType type)
    {
        bool isOwned = false;
        for (int i = 0; i < ownedTreeType.Count; i++)
        {
            if (ownedTreeType[i] == type)
                isOwned = true;
        }
        return isOwned;
    }

    /// <summary>
    /// 生成人类
    /// </summary>
    private IEnumerator GenerateHuman(float time)
    {
        if (!isGenerateHuman)
            isGenerateHuman = true;
        yield return new WaitForSeconds(time);
        GameObject human = Instantiate(Resources.Load<GameObject>("Prefabs/Human"), humanHomePos.position, Quaternion.identity);
        human.transform.SetParent(humanPos);
        if (humans.Count == 0)
            TriggerTipsAni("这片土地诞生了第一个人类");
        humans.Add(human);
    }

    /// <summary>
    /// 游戏初始
    /// </summary>
    private void GameAwake()
    {
        TriggerTipsAni("这好像又是一个新的开始");
        AudioManager.Instance.PlayGameBgm();
    }

    /// <summary>
    /// 游戏结束
    /// </summary>
    private void GameOver()
    {
        overMask.SetActive(true);
        Invoke("LoadScenes", 2f);
    }

    /// <summary>
    /// 重新加载场景
    /// </summary>
    private void LoadScenes()
    {
        SceneManager.LoadScene(0);
    }
}
