﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 树
/// </summary>
public class Tree : MonoBehaviour
{
    // 树的类型
    public SeedType treeType;
    // 树的生命值
    public float treeHP;
    // 树生产的木头数量
    public int wood;
    // 攻击者
    public List<GameObject> attacker;

    // 遮罩
    private GameObject greenMask;
    private GameObject shadow;
    // 树的动画
    private Animator treeAni;

    private void Awake()
    {
        greenMask = transform.Find("GreenMask").gameObject;
        shadow = transform.Find("Shadow").gameObject;
        treeAni = transform.Find("Tree").gameObject.GetComponent<Animator>();
    }

    /// <summary>
    /// 添加种植树的数量
    /// </summary>
    public void AddPlantTrees()
    {
        if (treeType != SeedType.FristSeed && GameManager.Instance.plantTrees.Count == 0)
        {
            treeAni.SetBool("Die", true);
            GameManager.Instance.plantTrees.Remove(gameObject);
            Destroy(greenMask);
            Destroy(shadow);
        }
        else
        {
            GameManager.Instance.plantTrees.Add(gameObject);
        }
    }

    /// <summary>
    /// 受到伤害
    /// </summary>
    public void Damage(float value)
    {
        if (treeHP > value)
        {
            treeHP -= value;
            // 判断类型
            if (treeType == SeedType.Diva)
            {
                // 攻击人类
                if (Random.Range(0, 3) == 0)
                {
                    treeAni.SetTrigger("Attack");
                    AudioManager.Instance.PlayGameSound(AudioManager.Instance.auioClips[6]);
                    AttackHuman();
                }
                else
                {
                    treeAni.SetTrigger("Damage");
                }
            }
            else
            {
                treeAni.SetTrigger("Damage");
            }
        }
        else
        {
            treeAni.SetBool("Die", true);
            AudioManager.Instance.PlayGameSound(AudioManager.Instance.auioClips[4]);
            Die();
        }
    }

    /// <summary>
    /// 攻击人类
    /// </summary>
    private void AttackHuman()
    {
        if (attacker.Count != 0)
        {
            foreach(GameObject g in attacker)
            {
                g.GetComponent<Human>().BeAttack();
            }
        }
    }

    /// <summary>
    /// 树被砍掉
    /// </summary>
    private void Die()
    {
        if (attacker.Count != 0)
        {
            // 第一个攻击者
            attacker[0].GetComponent<Human>().wood = wood;
            attacker[0].GetComponent<Human>().GoHome();
            // 其他攻击者
            for(int i = 1; i < attacker.Count; i++)
            {
                attacker[i].GetComponent<Human>().ChangeTarget();
            }
        }
        // 销毁自身属性
        GameManager.Instance.plantTrees.Remove(gameObject);
        Destroy(greenMask);
        Destroy(shadow);
    }

}
