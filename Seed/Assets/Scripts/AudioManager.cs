﻿using UnityEngine;

/// <summary>
/// 音效管理器
/// </summary>
public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    [Header("音效文件")]
    public AudioClip[] auioClips;
    [Header("游戏音乐")]
    public AudioSource bgmAudio;

    private void Awake()
    {
        Instance = this;
    }

    /// <summary>
    /// 播放游戏音乐
    /// </summary>
    public void PlayGameBgm()
    {
        bgmAudio.Play();
    }

    /// <summary>
    /// 播放游戏音效
    /// </summary>
    public void PlayGameSound(AudioClip clip)
    {
        AudioSource.PlayClipAtPoint(clip, new Vector3(0f, 0f, -10f));
    }

}
