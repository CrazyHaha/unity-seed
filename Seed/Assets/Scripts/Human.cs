﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 人类状态
/// </summary>
public enum HumanState
{
    // 游荡
    GoNone,
    // 回家
    GoHome,
    // 找树
    GoTree,
    // 攻击
    Attack,
    // 死亡
    Die,
}

/// <summary>
/// 人类控制脚本
/// </summary>
public class Human : MonoBehaviour
{
    // 生命值
    public float HP = 100;
    // 移动速度
    public float speed = 1;
    // 攻击力度
    public float attack = 1;
    // 目标
    public GameObject target;
    // 目标位置
    public Vector3 targetPos;
    // 当前位置
    public Vector3 currentPos;
    // 移动方向
    public Vector3 moveDirection;
    // 收获的木头数量
    public int wood;
    // 人类状态
    public HumanState humanState = HumanState.GoNone;
    // 老化速度
    private int agingRate = 1;
    // 动画控制器
    private Animator humanAni;
    // 人类图片属性
    private SpriteRenderer humanSprite;
    // 是否有目标
    private bool IsNoTarget = true;

    private void Awake()
    {
        // 获取相关组件
        humanAni = GetComponent<Animator>();
        humanSprite = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        // 状态检测
        if (GameManager.Instance.plantTrees.Count > 0 && humanState == HumanState.GoNone)
        {
            humanState = HumanState.GoTree;
        }
        if (humanState == HumanState.GoHome && target != GameManager.Instance.humanHomePos.gameObject)
        {
            humanAni.SetBool("Attack", false);
            humanAni.SetBool("GoHome", true);
            target = GameManager.Instance.humanHomePos.gameObject;
            targetPos = target.transform.position;
        }
        if (humanState != HumanState.Attack && humanSprite.color == Color.red)
        {
            humanSprite.color = Color.white;
        }
        // 更新目标
        UpdataTarget();
        // 人类移动
        MoveTowardsTarget();
        // 人类老化
        HumanAging();
    }


    /// <summary>
    /// 更新人类目标
    /// </summary>
    public void UpdataTarget()
    {
        switch (humanState)
        {
            case HumanState.GoNone:
                // 随机游荡
                if (targetPos == null || IsNoTarget)
                {
                    targetPos = new Vector3(Random.Range(0, 5), Random.Range(-2, 3), 0);
                    IsNoTarget = false;
                }
                break;
            case HumanState.GoTree:
                FindTargetTree();
                break;
        }
    }

    /// <summary>
    /// 向目标移动
    /// </summary>
    public void MoveTowardsTarget()
    {
        // 记录当前位置
        currentPos = transform.position;
        // 计算移动方向
        moveDirection = targetPos - currentPos;
        moveDirection.Normalize();
        // 设置人类图片方向
        if (currentPos.x > targetPos.x)
        {
            humanSprite.flipX = true;
        }
        else if (currentPos.x < targetPos.x)
        {
            humanSprite.flipX = false;
        }
        // 判断位置
        if (Mathf.Abs(currentPos.x - targetPos.x) < 0.25f && Mathf.Abs(currentPos.y - targetPos.y) < 0.15f)
        {
            IsNoTarget = true;
            if (humanState == HumanState.GoTree)
            {
                // 攻击目标
                humanState = HumanState.Attack;
                humanAni.SetBool("Attack", true);
                target.GetComponent<Tree>().attacker.Add(gameObject);
            }
            if (humanState == HumanState.GoHome && target == GameManager.Instance.humanHomePos.gameObject)
            {
                // 回到家
                GameManager.Instance.wood += wood;
                wood = 0;
                humanState = HumanState.GoNone;
                humanAni.SetBool("GoHome", false);
            }
        }
        else
        {
            if (humanState == HumanState.Attack || humanState == HumanState.Die)
                return;
            // 向目标移动
            transform.Translate(moveDirection * speed * 0.5f * Time.deltaTime);
        }

    }

    /// <summary>
    /// 查找目标树（距离最近的树）
    /// </summary>
    public void FindTargetTree()
    {
        float distance = 100;
        GameObject tree = null;
        foreach (GameObject g in GameManager.Instance.plantTrees)
        {
            float dis = Vector3.Distance(g.transform.position, transform.position);
            if (dis < distance)
            {
                distance = dis;
                tree = g;
            }
        }
        // 结果
        if (tree == target)
        {
            return;
        }
        else if (tree == null)
        {
            target = null;
            humanState = HumanState.GoNone;
        }
        else
        {
            target = tree;
            targetPos = target.transform.position + new Vector3(0, 0, -0.1f);
        }
    }

    /// <summary>
    /// 攻击目标（树）
    /// </summary>
    public void AttackTarget()
    {
        if (target == null)
            return;
        target.GetComponent<Tree>().Damage(attack);
        AudioManager.Instance.PlayGameSound(AudioManager.Instance.auioClips[3]);
    }

    /// <summary>
    /// 被攻击
    /// </summary>
    public void BeAttack()
    {
        StartCoroutine(AnimatorPause());
    }

    /// <summary>
    /// 更换目标（目标树被砍倒）
    /// </summary>
    public void ChangeTarget()
    {
        target = null;
        humanState = HumanState.GoNone;
        humanAni.SetBool("Attack", false);
    }

    /// <summary>
    /// 人类回家
    /// </summary>
    public void GoHome()
    {
        humanState = HumanState.GoHome;
        humanAni.SetBool("Attack", false);
        humanAni.SetBool("GoHome", true);
    }

    /// <summary>
    /// 人类死亡
    /// </summary>
    public void Die()
    {
        Destroy(gameObject);
    }

    /// <summary>
    /// 加速老化
    /// </summary>
    public void QuickAging()
    {
        StartCoroutine(Aging());
    }

    /// <summary>
    /// 加速老化协程
    /// </summary>
    private IEnumerator Aging()
    {
        while (HP > 0)
        {
            agingRate += 10;
            yield return new WaitForSeconds(1);
        }
    }

    /// <summary>
    /// 人类老化
    /// </summary>
    private void HumanAging()
    {
        // 老化
        if (HP > 0)
        {
            HP -= Time.deltaTime * agingRate;
        }
        // 死亡
        else if (humanState != HumanState.Die)
        {
            if (target != null && humanState == HumanState.Attack)
            {
                target.GetComponent<Tree>().attacker.Remove(gameObject);
            }
            humanAni.SetTrigger("Die");
            humanState = HumanState.Die;
            GameManager.Instance.humans.Remove(gameObject);
        }
    }

    /// <summary>
    /// 暂停行动动画
    /// </summary>
    private IEnumerator AnimatorPause()
    {
        Color color = humanSprite.color;
        humanAni.speed = 0;
        humanSprite.color = Color.red;
        yield return new WaitForSeconds(2);
        humanSprite.color = color;
        humanAni.speed = 1;
    }
}
