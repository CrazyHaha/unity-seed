﻿using UnityEngine;

/// <summary>
/// 种子类型
/// </summary>
public enum SeedType
{
    // 第一颗种子
    FristSeed,
    // 普通树
    Linbum,
    // 普通树2
    Linbum2,
    // 石头树
    StoneTree,
    // 花树
    Blumen,
    // 女歌手
    Diva,
    // 无
    None,
}

/// <summary>
/// 种子列表栏
/// </summary>
public class SeedItemBar : MonoBehaviour
{
    public static SeedItemBar Instance;

    // 所有树的实例图片
    public Sprite[] treeImgs;
    // 当前树的实例
    public GameObject currentTree;
    private SpriteRenderer currTreeImg;
    // 实例的初始位置
    private Vector3 originPos;
    // 是否显示树的实例
    private bool isShowTree;
    // 实例的颜色
    private Color originColor;

    // 当前种子类型
    private SeedType currSeedType;

    public SeedType CurrSeedType
    {
        get => currSeedType;
        set
        {
            if (currSeedType == value)
                return;
            currSeedType = value;
            SeedTypeSelect(value);
        }
    }

    private void Awake()
    {
        Instance = this;

        // 初始化实例类型
        currSeedType = SeedType.None;

        isShowTree = false;
        originPos = currentTree.transform.position;
        currTreeImg = currentTree.GetComponent<SpriteRenderer>();
        originColor = currTreeImg.color;
    }

    private void Update()
    {
        // 取消选择
        if (Input.GetMouseButtonDown(1))
        {
            isShowTree = false;
            GameManager.Instance.isReady = false;
        }
        // 显示树的实例
        if (isShowTree)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (currSeedType == SeedType.FristSeed)
                currentTree.transform.position = new Vector3(mousePos.x, mousePos.y + 0.8f, 0);
            else if (currSeedType == SeedType.Blumen)
                currentTree.transform.position = new Vector3(mousePos.x, mousePos.y + 0.4f, 0);
            else if (currSeedType == SeedType.Diva)
                currentTree.transform.position = new Vector3(mousePos.x, mousePos.y + 0.2f, 0);
            else
                currentTree.transform.position = new Vector3(mousePos.x, mousePos.y + 0.5f, 0);
            // 实例的颜色变换
            if (GameManager.Instance.isPlant && currTreeImg.color != originColor)
            {
                currTreeImg.color = originColor;
            }
            else if (!GameManager.Instance.isPlant && currTreeImg.color != Color.red)
            {
                currTreeImg.color = Color.red;
            }
        }
        else if (!isShowTree && currentTree.transform.position != originPos)
        {
            currentTree.transform.position = originPos;
            CurrSeedType = SeedType.None;
        }
        // 隐藏树的实例
        if (!GameManager.Instance.isReady && isShowTree)
        {
            isShowTree = false;
        }
    }


    /// <summary>
    /// 种子类型选择
    /// </summary>
    private void SeedTypeSelect(SeedType type)
    {
        if (type == SeedType.None)
            return;
        // 根据种子类型设置实例
        switch (type)
        {
            case SeedType.FristSeed:
                currTreeImg.sprite = treeImgs[0];
                break;
            case SeedType.Linbum:
                currTreeImg.sprite = treeImgs[1];
                break;
            case SeedType.Linbum2:
                currTreeImg.sprite = treeImgs[2];
                break;
            case SeedType.Blumen:
                currTreeImg.sprite = treeImgs[3];
                break;
            case SeedType.StoneTree:
                currTreeImg.sprite = treeImgs[4];
                break;
            case SeedType.Diva:
                currTreeImg.sprite = treeImgs[5];
                break;
        }
        isShowTree = true;
        GameManager.Instance.isReady = true;
    }

}
