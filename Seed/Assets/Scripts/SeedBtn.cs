﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 种子列表按钮
/// </summary>
public class SeedBtn : MonoBehaviour, IPointerClickHandler
{
    // 种子类型
    public SeedType seedType;
    // CD遮罩
    public Image CDMask;
    // CD时间
    public float CDTime;
    // CD是否结束
    private bool isCDOver = true;
    // 鼠标左键点击
    public UnityEvent leftClick;

    private void Awake()
    {
        leftClick.AddListener(new UnityAction(OnThisSeedClick));
    }

    private void Update()
    {
        // 是否隐藏第一颗种子选项
        if (seedType == SeedType.FristSeed)
        {
            if (GameManager.Instance.isStart && gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// 鼠标点击事件
    /// </summary>
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
            leftClick.Invoke();
    }

    /// <summary>
    /// 种子自身点击事件
    /// </summary>
    private void OnThisSeedClick()
    {
        if (!isCDOver)
            return;
        SeedItemBar.Instance.CurrSeedType = seedType;
    }

    /// <summary>
    /// 进入CD时间
    /// </summary>
    public void StartCD()
    {
        StartCoroutine(CD());
    }

    /// <summary>
    /// CD时间计算协程
    /// </summary>
    IEnumerator CD()
    {
        isCDOver = false;
        CDMask.fillAmount = 1;
        while (CDMask.fillAmount > 0)
        {
            yield return new WaitForSeconds(0.1f);
            CDMask.fillAmount -= 1 / (CDTime / 0.1f);
        }
        isCDOver = true;
    }

}
